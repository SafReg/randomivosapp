﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Navig : MonoBehaviour
{
	
	public GameObject canvasKezdo;
	public GameObject canvasKetcsapat;
	public GameObject canvasKetcsapatnevek;
	public GameObject canvasosszegzes;
	public GameObject canvasPontkerdez;
	public GameObject canvas1pont;
	public GameObject canvas2pont;
	public GameObject canvas3pont;
	public GameObject canvasfutangyoz;
	public GameObject canvasfutanveszt;
	public GameObject canvas2csPontkerdez;
	public GameObject canvas2cs1pont;
	public GameObject canvas2cs2pont;
	public GameObject canvas2cs3pont;
	public GameObject canvas2csfutangyoz;
	public GameObject canvas2csfutanveszt;
	public GameObject canvasallas;
	public GameObject canvasallas2;
	
	
    void Start()
    {
        SetKezdoActive(true);
		SetKetcsapatActive(false);
		SetKetcsapatnevekActive(false);
		SetosszegzesActive(false);
		SetPontkerdezActive(false);
		Set1pontActive(false);
		Set2pontActive(false);
		Set3pontActive(false);
		SetfutangyozActive(false);
		SetfutanvesztActive(false);
		Set2csPontkerdezActive(false);
		Set2cs1pontActive(false);
		Set2cs2pontActive(false);
		Set2cs3pontActive(false);
		Set2csfutangyozActive(false);
		Set2csfutanvesztActive(false);
		SetallasActive(false);
		Setallas2Active(false);
		
		
		
    }
	
	public void SetKezdoActive(bool active)
	{
		canvasKezdo.SetActive(active);
	}
	
	public void SetKetcsapatActive(bool active)
	{
		canvasKetcsapat.SetActive(active);
	}
	
	public void SetKetcsapatnevekActive(bool active)
	{
		canvasKetcsapatnevek.SetActive(active);
	}
	
	public void SetPontkerdezActive(bool active)
	{
		canvasPontkerdez.SetActive(active);
	}
	
	public void Set1pontActive(bool active)
	{
		canvas1pont.SetActive(active);
	}
	
	public void Set2pontActive(bool active)
	{
		canvas2pont.SetActive(active);
	}
	
	public void Set3pontActive(bool active)
	{
		canvas3pont.SetActive(active);
	}
	public void SetfutangyozActive(bool active)
	{
		canvasfutangyoz.SetActive(active);
	}
	public void SetfutanvesztActive(bool active)
	{
		canvasfutanveszt.SetActive(active);
	}
	public void SetosszegzesActive(bool active)
	{
		canvasosszegzes.SetActive(active);
	}
	
	public void Set2csPontkerdezActive(bool active)
	{
		canvas2csPontkerdez.SetActive(active);
	}
	
	public void Set2cs1pontActive(bool active)
	{
		canvas2cs1pont.SetActive(active);
	}
	
	public void Set2cs2pontActive(bool active)
	{
		canvas2cs2pont.SetActive(active);
	}
	
	public void Set2cs3pontActive(bool active)
	{
		canvas2cs3pont.SetActive(active);
	}
	public void Set2csfutangyozActive(bool active)
	{
		canvas2csfutangyoz.SetActive(active);
	}
	public void Set2csfutanvesztActive(bool active)
	{
		canvas2csfutanveszt.SetActive(active);
	}
	public void SetallasActive(bool active)
	{
		canvasallas.SetActive(active);
	}
	public void Setallas2Active(bool active)
	{
		canvasallas2.SetActive(active);
	}
	
    void Update()
    {
        
    }
}
