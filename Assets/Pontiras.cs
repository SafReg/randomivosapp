﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Pontiras : MonoBehaviour

{
	public TMP_Text lekerdezendo1;
	public TMP_Text lekerdezendo4;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	public void noveles1(TMP_Text lekerdezend)
	{
		string multbelipont = lekerdezend.text;
		int multbelipontszam = int.Parse(multbelipont);
		multbelipontszam += 1;
		lekerdezend.SetText(multbelipontszam.ToString());
	}
	public void noveles2(TMP_Text lekerdezend)
	{
		string multbelipont = lekerdezend.text;
		int multbelipontszam = int.Parse(multbelipont);
		multbelipontszam += 2;
		lekerdezend.SetText(multbelipontszam.ToString());
	}
	public void noveles3(TMP_Text lekerdezend)
	{
		string multbelipont = lekerdezend.text;
		int multbelipontszam = int.Parse(multbelipont);
		multbelipontszam += 3;
		lekerdezend.SetText(multbelipontszam.ToString());
	}
	
	public void masolas(TMP_Text masolando)
	{
		string masolat = lekerdezendo1.text;
		masolando.SetText(masolat);
	}
	public void masolas2(TMP_Text masolando)
	{
		string masolat = lekerdezendo4.text;
		masolando.SetText(masolat);
	}
}
