﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class csapatkiiras : MonoBehaviour
{
	public TMP_Text egycsnev;
	public TMP_Text ketcsnev;
	public TMP_Text ahovakell;
	public TMP_Text ahovakell2;
    // Start is called before the first frame update
    void Start()
    {
		//csereplusszsors(ahovakell);
		//csereplusszsors2(ahovakell2);
    }
	
	public void csereplusszsors(TMP_Text ahovakel)
	{
		string egyikneve = egycsnev.text;
		string masikneve = ketcsnev.text;
		string[] mindkettoneve = new string[] {egyikneve, masikneve};
		
		ahovakel.SetText("Szóval mindenki készen áll? Akkor egy játékos a(z) "
		+ mindkettoneve[Random.Range(0,1)] + 
		" csapatból válasszon hány pontos kérdést szeretne.");
	}
	public void csereplusszsors2(TMP_Text ahovakel)
	{
		string egyikneve = egycsnev.text;
		string masikneve = ketcsnev.text;
		string[] mindkettoneve = new string[] {egyikneve, masikneve};
		
		ahovakel.SetText("Szóval mindenki készen áll? Akkor egy játékos a(z) "
		+ mindkettoneve[Random.Range(1,2)] + 
		" csapatból válasszon hány pontos kérdést szeretne.");
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
