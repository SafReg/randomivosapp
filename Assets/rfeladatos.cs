﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class rfeladatos : MonoBehaviour
{
	public TMP_Text feladathely;
    // Start is called before the first frame update
    void Start()
    {
      
	  
	  //randomiras(feladathely);
	  
	}
	  
	  
	  public void randomiras1(TMP_Text anjam1)
	{	
		string[] egyesfeladatok = new string[] {
			"Igyál meg egy felest szívószállal", 
			"Harapj bele egy fej hagymába",
			"Nyomj 20 fekvőtámaszt", 
			"Találj bele 10 lépésről egy zsebkendővel a szemetesbe", 
			"Mutogasd el a nemi erőszak szókapcsolatot 1 perc alatt"};
		anjam1.SetText(egyesfeladatok[Random.Range(0,5)]);
	}
	
	
	public void randomiras2(TMP_Text anjam2)
	{	
		string[] kettesfeladatok = new string[] {
			"Játssz az ellenfél csapattagjaival kő-papír-ollót. Ha többet nyersz mint veszítesz, sikerült a feladat", 
			"Karaokezz egy az ellenfél csapata által választott dalt",
			"Igyál 3 felest 10 másodperc alatt", 
			"Meséld el miért sírtál utoljára",
			"Küldd el a Drinkyappot instasztoriban a picsába, és mondd el hogy egy jobb"};
		anjam2.SetText(kettesfeladatok[Random.Range(0,5)]);
	}
	
	
	public void randomiras3(TMP_Text anjam3)
	{	
		string[] harmasfeladatok = new string[] {
			"Kopogj be a szomszédhoz és kérj kölcsön egy óvszert", 
			"Pontozd a többieket egy 1-10-es skálán",
			"Győzz meg valakit, hogy engedje meg hogy mustárt kenj valamelyik testrészére, és nyald le onnan", 
			"Igyál 5 felest 13 másodperc alatt",
			"Sztriptízelj a Ducktales főcímdalára (alsónemű maradhat, na)"};
		anjam3.SetText(harmasfeladatok[Random.Range(0,5)]);
	}
    

    // Update is called once per frame
    void Update()
    {
        
    }
}
